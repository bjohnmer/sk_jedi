Rails.application.routes.draw do

  post "/deleteTicket" , to: "tickets#deleteTicket"
  get "/getTicket" , to: "tickets#getTicket"

  get "/imagecapcha" , to: "recapcha#image"
	post "/verifycapcha" , to: "recapcha#verifycapcha" 
  post "/deletecapcha" , to: "recapcha#deletecapcha" 
	get "/makecapcha" , to: "recapcha#makecapcha"
  
	get "/recapcha" , to: "recapcha#index" 
  post "/checking", to: "ads#checking"
  get "/allads/:id" , to: "ads#allads"

  post "/ads" , to: "ads#index"
  # post "ads/show" , to: "ads#show"
  #resources :ads, only: [ :index , :show ]
  get "/random" , to: "client#random"
  post "/create_viewer" , to: "client#create_viewer"
  post "/checksession", to: "client#checksession"
  get "/clientip" , to: "client#clientip", as: :see_clientip


  
end
