json.array!(@ads_answers_questions) do |ads_answers_question|
  json.extract! ads_answers_question, :id
  json.url ads_answers_question_url(ads_answers_question, format: :json)
end
