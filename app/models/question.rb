class Question < ActiveRecord::Base
	belongs_to :category
	has_many :ads_answers_question
	has_many :ads, through: :ads_answers_question
end
