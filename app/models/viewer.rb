class Viewer < ActiveRecord::Base
	has_many :ads_viewers , dependent: :destroy
	has_many :ads , through: :ads_viewers

	validates :session_id, uniqueness: true
end
