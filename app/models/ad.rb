class Ad < ActiveRecord::Base

	has_many :ads_answers_question , dependent: :destroy
	has_many :questions, through: :ads_answers_question
	has_many :answers, through: :ads_answers_question

	has_attached_file :file, styles: { medium: "800x800>", thumb: "300x300>" }
	
	validates_attachment_content_type :file, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]


	scope :moderated, -> {where(moderated: 1)}
	scope :actived, -> {where(status: 'actived')}
	scope :paid, -> {where(paid: 1)}
	
	def file_thumb
		self.file.url( :thumb )
	end
	
	def file_medium
		self.file.url( :medium )
	end

	def file_full
		self.file.url()
	end

	def save_in_local
		#  a = Ad.new
		self.file = URI.parse(self.image_url).open
		# a.file_file_name
		# a.file_file_content_type
		# a.account_d = numero
		# a.advertizer_id = numero
		self.save
		#
		
	end

end
