class AdsAnswersQuestion < ActiveRecord::Base
  belongs_to :ad 
  belongs_to :answer 
  belongs_to :question
end
