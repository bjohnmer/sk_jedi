class AdsViewer < ActiveRecord::Base
  belongs_to :ad
  belongs_to :viewer
  after_create :addOne

private
  def addOne
  	self.ad.update_attribute(:views, self.ad.views + 1)
  	self.viewer.update_attribute(:visits , self.viewer.visits + 1)
  end
end
