class Recapcha < ActiveRecord::Base
	 validates :answer, uniqueness: true
	 after_destroy :delete_image

	 private
	 def delete_image
	 	
	 	root_url = "#{Rails.root}/public/"
	 	File.delete(root_url + self.image_url) if File.exist? (root_url + self.image_url)
	 end
end
