   class AdsController < ApplicationController
  # cache = ActiveSupport::Cache::MemoryStore.new(expires_in: 1.minutes)
  protect_from_forgery
  skip_before_action :verify_authenticity_token, :authenticate,  if: :json_request? 
  after_action :addOneView, only: [:index]
  # GET /ads
  # GET /ads.json
  def index

    # Rails.cache.delete('jedi_cache_ads')
    # raise Rails.cache.read('jedi_cache_ads').to_yaml
    if Rails.cache.read('jedi_cache_ads') != nil
      # raise "el Cache existe".to_yaml
      @ad = Rails.cache.read('jedi_cache_ads')
    else
      @adver = Ad.all.moderated.paid.actived.limit(1).order("rand()").first
      Rails.cache.write("jedi_cache_ads", @adver, :expires_in => 1.minutes)
      @ad = Rails.cache.read('jedi_cache_ads')
    end
    # @ad.save_in_local

      aaq = AdsAnswersQuestion.where(ad_id: @ad.id).order("rand()").first
      question = Question.where(id: aaq.question_id).select('id','question','category_id').take
      answers = Answer.where(category_id: question.category_id ).where.not(id: aaq.answer_id).limit(3).select('id','answer','category_id') << Answer.where(category_id: question.category_id ).where(id: aaq.answer_id).select('id','answer','category_id').take
      answers = answers.shuffle
    #
    @objectad = {
      ad: @ad.as_json,
      objquestion: question.as_json,
      objanswers: answers.shuffle.as_json,
    }
    render json: @objectad 
  end

  def allads

    # raise viewer_params[:ad_id].present?.to_yaml
    # if viewer_params[:ad_id].present?
    #   @ad= Ad.find(viewer_params[:ad_id])
    # else
    # end
      @ad= Ad.find(params[:id])
      aaq = AdsAnswersQuestion.where(ad_id: @ad.id).order("rand()").first
      question = Question.where(id: aaq.question_id).select('id','question','category_id').take
      answers = Answer.where(category_id: question.category_id ).where.not(id: aaq.answer_id).limit(3).select('id','answer','category_id') << Answer.where(category_id: question.category_id ).where(id: aaq.answer_id).select('id','answer','category_id').take
      answers = answers.shuffle
    #
    @objectad = {
      ad: @ad.as_json,
      objquestion: question.as_json,
      objanswers: answers.shuffle.as_json,
    }
    
    render json: @objectad.as_json 
    end

  # POST /checking
  # POST /checking.json 
  def checking
    aaq = AdsAnswersQuestion.where(ad_id: ad_params[:ad_id] , answer_id: ad_params[:answer_id] , question_id: ad_params[:question_id] ).present?
    @check = {
      status: aaq,
    }
    render json: @check
  end

  def addOneView
    if @ad.present?
     
      if viewer_params[:session_id].present?
        @viewer =  Viewer.where(session_id: viewer_params[:session_id]).take
        @ad_viewer = AdsViewer.create!(ad: @ad , viewer: @viewer  )
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    # Never trust parameters from the scary internet, only allow the white list through.
    #def ad_params
    #  params.fetch(:ad, {})
    #end
    def ad_params
      params.require(:aaq).permit(:ad_id, :answer_id, :question_id)
    end

    def viewer_params
      params.require(:viewer_data).permit(:ad_id, :session_id)
    end

    def json_request?
      request.format.json?
    end
end
