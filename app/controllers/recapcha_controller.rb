class RecapchaController < ApplicationController
	protect_from_forgery
  	skip_before_action :verify_authenticity_token, :authenticate,  if: :json_request? 
	def index
		@capchas = Recapcha.all
		render json: @capchas
	end
	def makecapcha
		answer = rand(36**5).to_s(36)

		img = Magick::Image.read("#{Rails.root}/public/images/bg_human.png").first
	    mark = Magick::Image.new(300, 75) {self.background_color = "none"}
	    draw = Magick::Draw.new
	    name = rand(36**30).to_s(36)
	    draw.annotate(mark, 0, 0, 0, 0, answer) do
	      draw.gravity = Magick::CenterGravity
	      draw.pointsize = 30
				fontArray = ["/usr/share/ghostscript/9.19/Resource/Font/NimbusMono-Bold",
				 "/usr/share/ghostscript/9.19/Resource/Font/NimbusRomNo9L-Med",
				 "/usr/share/ghostscript/9.19/Resource/Font/NimbusSanL-ReguCondItal",
				 "/usr/share/ghostscript/9.19/Resource/Font/URWGothicL-Demi",
				 "/usr/share/ghostscript/9.19/Resource/Font/URWPalladioL-Ital",
				 "/usr/share/ghostscript/9.19/Resource/Font/CenturySchL-Roma"]
	 			pos = rand(0..5)
	      draw.font = fontArray[pos] # set font
	      draw.fill = "white"
	      draw.stroke = "white"
	    end

	    rotate = rand(-30..+30)
	    mark = mark.rotate(rotate)

	    img3 = img.dissolve(mark, 1, 1, Magick::CenterGravity)
	    dirandname = "images/human/human_#{name}.jpg"
	    img3.write("#{Rails.root}/public/#{dirandname}")

		 @capcha = Recapcha.new(answer: answer, image_url: dirandname)
	      if @capcha.save
	        render json: @capcha , status: :created
	      else
	        render json: @capcha.errors, status: :unprocessable_entity 
	      end
	end

	def deletecapcha
		
		@capcha = Recapcha.where(answer: capcha_params[:answer]).take
		@capcha.destroy if @capcha
	     render json: @capcha
	end

	def verifycapcha
		rec = Recapcha.where(answer: capcha_params[:answer]).present?
	    @capcha = {
	      status: rec,
	    }
	    render json: @capcha
	end

	private
	   	def capcha_params
	      params.require(:capcha_data).permit( :answer )
	    end
	    def json_request?
	      request.format.json?
	    end
end
