class TicketsController < ApplicationController
	protect_from_forgery
  	skip_before_action :verify_authenticity_token, :authenticate,  if: :json_request? 

  def getTicket
    @ticket = Ticket.all.limit(1).select('id','ticket').order("rand()").first
    render json: @ticket
  end
  def deleteTicket
  	 @ticket = Ticket.where(ticket: ticket_params[:ticket]).take
	   @ticket.destroy if @ticket
      render json: @ticket
  end

private

	def ticket_params
	  params.require(:ticket_data).permit(:ticket )
	end
	def json_request?
      request.format.json?
    end

end