class ClientController < ApplicationController
	protect_from_forgery
  	skip_before_action :verify_authenticity_token, :authenticate,  if: :json_request? 
	def clientip
	    @client_data = {
	      client_ip: request.env['HTTP_X_REAL_IP'],
	      user_agent: request.env["HTTP_USER_AGENT"],
	    }
		render json: @client_data
	end

	def create_viewer
		 @viewer = Viewer.new(session_id: client_params[:hashed][:session_id] , user_agent: client_params[:hashed][:user_agent], ip_client: client_params[:hashed][:ip] , created_at: client_params[:hashed][:creation_date])
	      if @viewer.save
	        render json: @viewer , status: :created
	      else
	        render json: @viewer.errors, status: :unprocessable_entity 
	      end
	end

	def checksession

		viewer = Viewer.where(session_id: session_params[:session_id]).take
		if viewer.present?
			@datos = {
				present: true,
				status: viewer.status,
			}
		else
			@datos = {
				present: false
			}
		end
		render json: @datos
		
	end

	def random
		pos = rand(0..5)
		 raise pos.to_yaml
	end

	private
		def client_params
      		params.require(:padawan)
    	end
    	def session_params
    		params.require(:session).permit(:session_id)
    	end
	 	def json_request?
	      request.format.json?
	    end
end
